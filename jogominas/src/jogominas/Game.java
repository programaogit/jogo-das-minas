package jogominas;



public class Game {
	
    private Board board;
    boolean finish = false;
    boolean win = false;
    int turn=0;
    
    public Game(){
        board = new Board();
        Play(board);
        
    }
    
    public void Play(Board board){
        System.out.println("**********Bem vindo ao MineSweeper!**********");
        System.out.println("");
        do{
            turn++;
            System.out.println("Turno "+turn);
            board.show();
            finish = board.setPosition();
            
            if(!finish){
                board.openNeighbors();
                finish = board.win();
            }
            
        }while(!finish);
        
        if(board.win()){
            System.out.println("Parab�ns! Nao calhou em nenhuma Mina em "+turn+" turnos");
            board.showMines();
        } else {
            System.out.println("BOOM! Perdeu");
            board.showMines();
        }
    }
}